# Afar translation for lomiri-system-settings-phone
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the lomiri-system-settings-phone package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-phone\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-18 11:12-0600\n"
"PO-Revision-Date: 2015-07-19 00:16+0000\n"
"Last-Translator: Charif AYFARAH <ayfarah@ymail.com>\n"
"Language-Team: Afar <aa@li.org>\n"
"Language: aa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2015-07-21 05:34+0000\n"
"X-Generator: Launchpad (build 17628)\n"

#: ../plugins/phone/CallForwarding.qml:240
msgid "Cancel"
msgstr "Bayis"

#: ../plugins/phone/CallForwarding.qml:253
msgid "Set"
msgstr "Massos"

#. TRANSLATORS: This is a keyword or name for the phone plugin which is used while searching
#: ../plugins/phone/PageComponent.qml:32
#: ../build/po/settings.js:250
msgid "Phone"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:305
msgid "OK"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:45 ../plugins/phone/MultiSim.qml:54
#: ../plugins/phone/NoSims.qml:28 ../plugins/phone/SingleSim.qml:42
msgid "Call forwarding"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:131
msgid "Forward every incoming call"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:146
msgid "Redirects all phone calls to another number."
msgstr ""

#: ../plugins/phone/CallForwarding.qml:158
msgid "Call forwarding status can’t be checked "
msgstr ""

#: ../plugins/phone/CallForwarding.qml:166
msgid "Forward incoming calls when:"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:175
msgid "I’m on another call"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:186
msgid "I don’t answer"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:197
#, fuzzy
msgid "My phone is unreachable"
msgstr "Modem makkaabise ma geytima"

#: ../plugins/phone/CallForwarding.qml:227
msgid "Contacts…"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:274
msgid "Please select a phone number"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:283
msgid "Numbers"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:302
msgid "Could not forward to this contact"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:303
msgid "Contact not associated with any phone number."
msgstr ""

#: ../plugins/phone/CallForwarding.qml:382
msgid "All calls"
msgstr ""

#: ../plugins/phone/CallForwarding.qml:384
#, fuzzy
msgid "Some calls"
msgstr "Telefoon seecooca:"

#: ../plugins/phone/CallForwarding.qml:386
msgid "Off"
msgstr ""

#. TRANSLATORS: This string will be truncated on smaller displays.
#: ../plugins/phone/CallForwardItem.qml:157
#: ../plugins/phone/CallForwardItem.qml:202
msgid "Forward to"
msgstr ""

#: ../plugins/phone/CallForwardItem.qml:171
msgid "Enter a number"
msgstr ""

#: ../plugins/phone/CallForwardItem.qml:218
msgid "Call forwarding can’t be changed right now."
msgstr ""

#: ../plugins/phone/CallWaiting.qml:31 ../plugins/phone/CallWaiting.qml:86
#: ../plugins/phone/MultiSim.qml:42 ../plugins/phone/NoSims.qml:34
#: ../plugins/phone/SingleSim.qml:34
msgid "Call waiting"
msgstr ""

#: ../plugins/phone/CallWaiting.qml:101
msgid ""
"Lets you answer or start a new call while on another call, and switch "
"between them"
msgstr ""

#: ../plugins/phone/MultiSim.qml:67 ../plugins/phone/NoSims.qml:42
msgid "Services"
msgstr ""

#: ../plugins/phone/PageComponent.qml:102
#, fuzzy
msgid "Dialpad tones"
msgstr "Kalluuwus-beytâ xongoloola"

#: ../plugins/phone/ServiceInfo.qml:109
#, qt-format
msgid "Last called %1"
msgstr ""

#: ../plugins/phone/ServiceInfo.qml:119
msgid "Call"
msgstr ""

#. TRANSLATORS: %1 is the name of the (network) carrier
#: ../plugins/phone/Services.qml:36 ../plugins/phone/SingleSim.qml:55
#, qt-format
msgid "%1 Services"
msgstr ""

#: ../plugins/phone/SingleSim.qml:29
msgid "SIM"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the phone plugin which is used while searching
#: ../build/po/settings.js:10
msgid "phone"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the phone plugin which is used while searching
#: ../build/po/settings.js:254
msgid "services"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the phone plugin which is used while searching
#: ../build/po/settings.js:256
msgid "forwarding"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the phone plugin which is used while searching
#: ../build/po/settings.js:258
msgid "waiting"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the phone plugin which is used while searching
#: ../build/po/settings.js:260
msgid "call"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the phone plugin which is used while searching
#: ../build/po/settings.js:262
msgid "dialpad"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the phone plugin which is used while searching
#: ../build/po/settings.js:264
msgid "shortcuts"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the phone plugin which is used while searching
#: ../build/po/settings.js:266
msgid "numbers"
msgstr ""

